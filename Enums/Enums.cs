﻿using System;

namespace Enums
{
    public enum Types
    {
        Painting,
        Sculpture,
        Fabric,
        Other,
    }
}
