﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ArtGal.Register;
using ArtGalleryBLL.InterfacesBLL;
using ArtGalleryDTO.EntitiesDTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ArtGal.Controllers
{
    [ApiController]
    [Route("api/selleraccount")]
    public class SellerAccountController : ControllerBase
    {
        private ISellerService _sellerService;
        public SellerAccountController(ISellerService sellerService)
        {
            this._sellerService = sellerService;

        }

        [HttpPost]
        public async Task<IActionResult> Register([FromBody]RegisterModel model)
        {

            var user = new SellerDTO
            {
                Id = model.Id,
                Name = model.Name,
                Address = model.Address,
                Account = model.Account,
                Email = model.Email,
                PasswordHash = model.PasswordHash,
                PasswordConfirm = model.PasswordConfirm
         
             };
            var data = await _sellerService.Add(user);
            return Ok(data);
        }
    }
}
