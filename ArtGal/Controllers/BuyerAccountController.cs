﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ArtGal.Register;
using ArtGalleryBLL.InterfacesBLL;
using ArtGalleryDTO.EntitiesDTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ArtGal.Controllers
{
    [ApiController]
    [Route("api/buyeraccount")]
    public class BuyerAccountContoller : ControllerBase
    {
       
        private IBuyerService _buyerService;
        public BuyerAccountContoller(IBuyerService buyerService)
        {
          this._buyerService = buyerService;
        }
       

        [HttpPost]
        public async Task<IActionResult> Register([FromBody]RegisterModel model)
        {

            var user = new BuyerDTO
            {
                
                Name = model.Name,
                Address = model.Address,
                Account = model.Account,
                Email = model.Email,
                PasswordHash = model.PasswordHash,
               // PasswordConfirm = model.PasswordConfirm
            };
            var data = await _buyerService.Add(user);
            return Ok(data);
        }
    }
}
