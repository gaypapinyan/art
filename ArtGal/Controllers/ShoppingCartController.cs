﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ArtGalleryBLL.InterfacesBLL;
using ArtGalleryDTO.EntitiesDTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ArtGal.Controllers
{
    [Route("api/shoppingcart")]
    [ApiController]
    public class ShoppingCartController : ControllerBase
    {
        private IShoppingCartService shoppingCart;

        public ShoppingCartController(IShoppingCartService shoppingCart)
        {
            this.shoppingCart = shoppingCart;
        }

        // GET: api/shoppingcart
        //[Route("~/api/Get")]
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                var data = await shoppingCart.GetAll();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }

        }

        // GET: api/shoppingcart/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {

            try
            {
                var data = await shoppingCart.GetById(id);
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }

        }


        // POST: api/shoppingcart
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] ShoppingCartDTO cart)
        {
            try
            {
                var data = await shoppingCart.Add(cart);
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        // PUT: api/shoppingcart/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] ShoppingCartDTO cart)
        {
            if (id == cart.Id)
            {
                shoppingCart.Update(cart);
                return Ok();
            }
            return BadRequest(id);
        }

        // DELETE: api/shoppingcart/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var data = await shoppingCart.GetById(id);
            if (data == null)
            {
                return BadRequest(id);
            }
            else
            {
                await shoppingCart.Remove(id);
                return Ok("Successfully deleted!");
            }

        }
    }
}
