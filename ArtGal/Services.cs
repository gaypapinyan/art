﻿using ArtGalleryBLL.InterfacesBLL;
using ArtGalleryBLL.ServicesBLL;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ArtGal
{
    static public class Services
    {
        public static IServiceCollection RegisterServices(this IServiceCollection services)
        {
            services.AddScoped<IArtObjectService, ArtObjectService>();
            services.AddScoped<IShoppingCartService, ShoppingCartService>();
            services.AddScoped<ISellerService, SellerService>();
            services.AddScoped<IBuyerService, BuyerService>();
            return services;
        }
    }
}
