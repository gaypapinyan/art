﻿using ArtGalleryDAL.Entities;
using ArtGalleryDAL.Interfaces;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ArtGal.Register
{
    public class RegisterModel
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        public string Account { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        [DataType(DataType.Password)]
        
        public string PasswordHash { get; set; }
        [Required]
        [Compare("PasswordHash", ErrorMessage = "wrong")]
        [DataType(DataType.Password)]
        public string PasswordConfirm { get; set; }

       
    }
}
