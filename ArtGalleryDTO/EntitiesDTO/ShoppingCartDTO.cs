﻿using ArtGalleryDTO.InterfacesDTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace ArtGalleryDTO.EntitiesDTO
{
    public class ShoppingCartDTO : IBaseEntityDTO
    {
        public int Id { get; set; }
        public int artObjectId { get; set; }
        public int buyerId { get; set; }

    }
}
