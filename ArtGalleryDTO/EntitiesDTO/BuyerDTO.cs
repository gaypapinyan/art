﻿using ArtGalleryDTO.InterfacesDTO;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ArtGalleryDTO.EntitiesDTO
{
    public class BuyerDTO : IBaseEntityDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Account { get; set; }
        public string Email { get; set; }
       
        public string PasswordHash { get; set; }
        public string PasswordConfirm { get; set; }

        
        public ICollection<ShoppingCartDTO> shoppingCart { get; set; }
    }
}
