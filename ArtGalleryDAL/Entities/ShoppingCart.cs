﻿using ArtGalleryDAL.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ArtGalleryDAL.Entities
{
    public class ShoppingCart : IBaseEntity
    {
        [Key]
        public int Id { get; set; }
        public int artObjectId { get; set; }
        public int buyerId { get; set; }

        [ForeignKey("buyerId")]
        public Buyer buyer { get; set; }

        [ForeignKey("artObjectId")]
        public ArtObject artObject { get; set; }
    }
}
